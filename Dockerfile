FROM node:16.19.0 as build

ARG HOME_DIR="app"

WORKDIR /$HOME_DIR

COPY . .

RUN npm install

RUN npm run build

EXPOSE 3000

CMD npm run start
