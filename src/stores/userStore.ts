import { create } from "zustand";
import { persist } from "zustand/middleware";
import { setCookie } from "typescript-cookie";
import {
  FundCredentialsType,
  UserCredentialsType,
  UserInfoType,
} from "../utils/typedefs";
import { getUser, signup } from "../utils/axios";
type UserSettingsStore = {
  userCredentials?: UserCredentialsType;
  fundCredentials?: FundCredentialsType;
  userInfo?: UserInfoType;
  fetching: boolean;
  fitnessAppAdded: boolean;
  checkUser: () => Promise<boolean>;
  setCredentials: (creds: UserCredentialsType | FundCredentialsType) => void;
  setCompanyCreds: (
    creds: Pick<UserCredentialsType, "company" | "unit_in_company">
  ) => void;
  signUpUser: () => Promise<boolean>;
  setFitnessAppAdded: (state: boolean) => void;
};

export const useUserSettingsStore = create<UserSettingsStore>()(
  persist(
    (set, get) => ({
      fetching: true,
      fitnessAppAdded: false,
      async checkUser() {
        /** @todo запросик к беку бебеку */
        const res = await getUser();
        if (res) {
          set((state) => ({ userInfo: res }));
          return true;
        } else {
          set((state) => ({ userInfo: undefined }));
          return false;
        }
      },
      setCredentials(creds) {
        if ("first_name" in creds) set((state) => ({ userCredentials: creds }));
        else if ("title" in creds) set((state) => ({ fundCredentials: creds }));
        setCookie("sessionId", "lol");
      },
      setCompanyCreds(creds) {
        set((state) => ({
          userCredentials: {
            ...(state.userCredentials as UserCredentialsType),
            company: creds.company,
            unit_in_company: creds.unit_in_company,
          },
        }));
      },
      setFitnessAppAdded(newState) {
        set((state) => ({ fitnessAppAdded: newState }));
      },
      async signUpUser() {
        const creds = get().userCredentials;
        if (creds) {
          const res = await signup(creds);
          if (res) {
            set((state) => ({ userCredentials: undefined }));
            return true;
          }
        }
        return false;
      },
    }),
    { name: "user-settings" }
  )
);
