import axios from "axios";
import { CredentialsType, UserCredentialsType, UserInfoType } from "./typedefs";
import { getCookie, setCookie } from "typescript-cookie";

export async function signup(creds: UserCredentialsType) {
  return axios
    .post(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/user/sign/up", creds, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      return res.data;
    });
}

export async function signin(
  creds: Pick<CredentialsType, "email" | "password">
) {
  return axios
    .post(
      process.env.REACT_APP_BACKEND_ENDPOINT + "/api/token/",
      { username: creds.email, password: creds.password },
      {
        headers: { "Content-Type": "application/json" },
      }
    )
    .then((res) => {
      if (res.data["access"]) {
        setCookie("access", res.data["access"]);
        setCookie("refresh", res.data["refresh"]);
        return true;
      }
      return false;
    });
}

export async function getCompanies() {
  return axios
    .get(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/companies")
    .then((res) => {
      if (res.status === 201) return true;
      return false;
    });
}

export async function getUser(): Promise<false | UserInfoType> {
  return axios
    .get(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/user", {
      headers: {
        Authorization: "Bearer " + getCookie("access"),
      },
    })
    .then((res) => {
      if (res.status === 200) return res.data;
      return false;
    })
    .catch(() => {
      return false;
    });
}

export async function getLink() {
  return axios
    .get(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/oauth/google/link")
    .then((res) => {
      if (res.status === 200) return res.data;
      return true;
    })
    .catch(() => {
      return true;
    });
}

export async function verifyFit(code: string) {
  return axios
    .post(
      process.env.REACT_APP_BACKEND_ENDPOINT + "/api/oauth/google/",
      { code },
      {
        headers: {
          Authorization: "Bearer " + getCookie("access"),
        },
      }
    )
    .then((res) => {
      if (res.status === 201) return true;
      return false;
    })
    .catch((res) => {
      return false;
    });
}

export async function getActivities() {
  return axios
    .get(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/user/activities", {
      headers: {
        Authorization: "Bearer " + getCookie("access"),
      },
    })
    .then((res) => {
      if (res.status === 200) return res.data;
      return false;
    })
    .catch((res) => {
      return false;
    });
}
