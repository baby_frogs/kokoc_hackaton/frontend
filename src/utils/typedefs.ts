export type CredentialsType = {
  type: "user" | "fund";
  email: string;
  password?: string;
};

export interface UserCredentialsType extends CredentialsType {
  last_name: string;
  first_name: string;
  unit_in_company?: string;
  company?: number;
  avaUrl?: string;
}

export interface FundCredentialsType extends CredentialsType {
  title: string;
  ogrn: number;
  inn: number;
  avaUrl?: string;
}

export type StoryType = {
  src: string;
};

export type CompanyType = {
  id: number;
  name: string;
};

export type UserInfoType = {
  first_name: string;
  last_name: string;
  type: number;
  unit_in_company: string;
  company: {
    id: number;
    name: string;
  };
};

export type ActivityType = {
  id: number;
  duration: number;
  activity_start_at: string;
  activity_end_at: string;
  calories: number;
};

export type AchievementType = {
  title: string;
  icon: string;
  description: string;
};
