import React, { PropsWithChildren, useEffect, useState } from "react";
import { useUserSettingsStore } from "../stores/userStore";
import { useNavigate } from "react-router-dom";

interface Props extends PropsWithChildren {}

function AuthWrapper({ children }: Props) {
  const checkUser = useUserSettingsStore((state) => state.checkUser);
  const [res, setRes] = useState<boolean>();
  const navigate = useNavigate();
  useEffect(() => {
    checkUser().then((res) => {
      if (res) setRes(res);
      else navigate("/greetings");
    });
  }, []);
  return res ? <>{children}</> : <></>;
}

export default AuthWrapper;
