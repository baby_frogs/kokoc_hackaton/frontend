import MainInfoButton from "../MainInfoButton";
import styles from "./earnedTodayButton.module.scss";
import goldenRuble from "../../assets/golden-ruble.png";

function EarnedTodayButton() {
  return (
    <MainInfoButton
      p={<p>Заработано за сегодня</p>}
      src="/mycoins"
      style={{ gridArea: "A", position: "relative" }}
    >
      <>
        <h1 className={styles.h1}>125</h1>
        <img
          src={goldenRuble}
          className={styles.coin}
          alt="изображение монеты"
        />
      </>
    </MainInfoButton>
  );
}

export default EarnedTodayButton;
