import styles from "./mainInfoButton.module.scss";
import { ReactComponent as ChevronRight } from "../../assets/chevronRight.svg";
import { Link } from "react-router-dom";
import { HTMLAttributes, PropsWithChildren, ReactElement } from "react";
export interface MainInfoButtonProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLAnchorElement> {
  p: ReactElement<HTMLParagraphElement>;
  src: string;
}

function MainInfoButton({
  p,
  src,
  children,
  className,
  ...attrs
}: MainInfoButtonProps) {
  return (
    <Link className={styles.container + " " + className} to={src} {...attrs}>
      <div className={styles.top}>
        {p}
        <ChevronRight />
      </div>
      {children}
    </Link>
  );
}

export default MainInfoButton;
