import React, { PropsWithChildren, useEffect, useState } from "react";
import { useUserSettingsStore } from "../stores/userStore";
import { Navigate } from "react-router-dom";

interface Props extends PropsWithChildren {}

function FitWrapper({ children }: Props) {
  const fitnessAppAdded = useUserSettingsStore(
    (state) => state.fitnessAppAdded
  );
  return fitnessAppAdded ? <>{children}</> : <Navigate to="/addFit" />;
}

export default FitWrapper;
