import React from "react";

type Props = { src?: string };

function Avatar({ src }: Props) {
  return <img src={src} alt="аватар пользователя" />;
}

export default Avatar;
