import BigButton from "../../UI/BigButton";
import styles from "./challengeContainer.module.scss";
import target from "../../assets/target.png";

function ChallengeContainer() {
  return (
    <div className={styles.container}>
      <img src={target} alt="изображение испытания" />
      <h2>Приключение в Гатчине</h2>
      <p>
        Пройдите расстояние в 45км, такое расстояние от Санкт-Петербурга до
        гатчины
      </p>
      <BigButton text="Принять" />
    </div>
  );
}

export default ChallengeContainer;
