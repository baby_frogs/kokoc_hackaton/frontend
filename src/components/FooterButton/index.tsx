import React, { PropsWithChildren } from "react";
import { Link } from "react-router-dom";
import styles from "./footerButton.module.scss";

interface Props extends PropsWithChildren {
  to: string;
  title: string;
  active?: boolean;
}

function FooterButton({ to, title, children, active = false }: Props) {
  return (
    <Link
      to={to}
      className={
        active ? styles.container + " " + styles.active : styles.container
      }
    >
      {children}
      <p>{title}</p>
    </Link>
  );
}

export default FooterButton;
