import styles from "./achievement.module.scss";
import MainButton from "../../UI/MainButton";
import { AchievementType } from "../../utils/typedefs";

type Props = {
  achievement: AchievementType;
};

function Achievement({ achievement }: Props) {
  return (
    <MainButton>
      <div className={styles.container}>
        <img
          className={styles.icon}
          src={achievement.icon}
          alt="иконка достижения"
        />
        <div className={styles.text}>
          <h3>{achievement.title}</h3>
          <p>{achievement.description}</p>
        </div>
      </div>
    </MainButton>
  );
}

export default Achievement;
