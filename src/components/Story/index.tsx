import { StoryType } from "../../utils/typedefs";
import styles from "./story.module.scss";

type Props = {
  story: StoryType;
};

function Story({ story }: Props) {
  return (
    <>
      <button className={styles.button}>
        <img src={story.src} alt="история" />
      </button>
      {false && (
        <div className={styles.overlay}>
          <div
            className={styles.container}
            style={{ backgroundImage: `url(${story.src})` }}
          >
            <div /** @todo lines for stories */></div>
          </div>
        </div>
      )}
    </>
  );
}

export default Story;
