import { useEffect, useState } from "react";
import { ActivityType } from "../../utils/typedefs";
import Activity from "../Activity";
import styles from "./activities.module.scss";
import { getActivities } from "../../utils/axios";

function Activities() {
  const [activities, setActivities] = useState<ActivityType[]>();

  useEffect(() => {
    getActivities().then((res) => res && setActivities(res));
  }, []);
  return (
    <div className={styles.container}>
      {activities &&
        activities.map((activity) => <Activity activity={activity} />)}
    </div>
  );
}

export default Activities;
