import { MainInfoButtonProps } from "../MainInfoButton";
import coin from "../../assets/golden-coin-small.png";
import styles from "./activity.module.scss";
import { ActivityType } from "../../utils/typedefs";
import MainButton from "../../UI/MainButton";
interface Props extends Partial<MainInfoButtonProps> {
  activity: ActivityType;
}

function Activity({ activity }: Props) {
  return (
    <MainButton>
      <div className={styles.container}>
        <div className={styles.left}>
          <p className={styles.type}>Тренировка</p>
          <p className={styles.info}>
            {activity.duration} минут • {activity.calories} калории
          </p>
          <p className={styles.time}>{activity.activity_start_at}</p>
        </div>
        <div className={styles.right}>
          <h2>
            +{" "}
            {Math.floor(
              (activity.calories / 600 + activity.duration / 60) * 100
            )}
          </h2>
          <img src={coin} alt="изображение монеты" />
        </div>
      </div>
    </MainButton>
  );
}

export default Activity;
