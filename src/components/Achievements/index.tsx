import { AchievementType } from "../../utils/typedefs";
import styles from "./achievements.module.scss";
import achievement1 from "../../assets/achievement1.png";
import achievement2 from "../../assets/achievement2.png";
import Achievement from "../Achievement";

const achievements: Readonly<AchievementType>[] = [
  {
    title: "Звон монет",
    description: "Пожертвовать 10 000 ₽",
    icon: achievement1,
  },
  {
    title: "Помощник",
    description: "Выполнить задание любого фонда",
    icon: achievement2,
  },
];
function Achievements() {
  return (
    <div className={styles.container}>
      {achievements.map((achievement) => (
        <Achievement achievement={achievement} />
      ))}
    </div>
  );
}

export default Achievements;
