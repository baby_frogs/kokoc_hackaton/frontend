import FooterButton from "../FooterButton";
import styles from "./footer.module.scss";
import { ReactComponent as MainSVG } from "../../assets/footerSVGs/main.svg";
import { ReactComponent as TasksSVG } from "../../assets/footerSVGs/tasks.svg";
import { ReactComponent as RatingSVG } from "../../assets/footerSVGs/rating.svg";

function Footer() {
  return (
    <div className={styles.container}>
      <div className={styles.buttons}>
        <FooterButton to="/main" title="Обзор">
          <MainSVG />
        </FooterButton>
        <FooterButton to="/tasks" title="Задания">
          <TasksSVG />
        </FooterButton>
        <FooterButton to="/rating" title="Рейтинг">
          <RatingSVG />
        </FooterButton>
      </div>
    </div>
  );
}

export default Footer;
