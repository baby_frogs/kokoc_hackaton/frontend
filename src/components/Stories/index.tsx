import Story from "../Story";
import styles from "./stories.module.scss";

function Stories() {
  return (
    <div className={styles.container}>
      <Story story={{ src: "" /** @todo подумоть что с историями*/ }} />
      <Story story={{ src: "" }} />
      <Story story={{ src: "" }} />
      <Story story={{ src: "" }} />
      <Story story={{ src: "" }} />
      <Story story={{ src: "" }} />
      <Story story={{ src: "" }} />
    </div>
  );
}

export default Stories;
