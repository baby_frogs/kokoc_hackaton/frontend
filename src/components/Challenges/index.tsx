import ChallengeContainer from "../ChallengeContainer";
import styles from "./challenges.module.scss";

function Challenges() {
  return (
    <div className={styles.container}>
      <ChallengeContainer />
      <ChallengeContainer />
      <ChallengeContainer />
    </div>
  );
}

export default Challenges;
