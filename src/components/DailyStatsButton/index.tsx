import styles from "./dailyStatsButton.module.scss";
import MainInfoButton from "../MainInfoButton";

function DailyStatsButton() {
  return (
    <MainInfoButton
      p={<p>Статистика за день</p>}
      src="/dailystats"
      style={{ gridArea: "C" }}
    >
      <div className={styles.bottom}>
        <div className={styles.block}>
          <h3>250</h3>
          <span>Калории</span>
        </div>
        <div className={styles.block}>
          <h3>1ч 15м</h3>
          <span>Время</span>
        </div>
        <div className={styles.block}>
          <h3>1</h3>
          <span>Разминка</span>
        </div>
      </div>
    </MainInfoButton>
  );
}

export default DailyStatsButton;
