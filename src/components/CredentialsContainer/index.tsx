import { Link } from "react-router-dom";
import { useUserSettingsStore } from "../../stores/userStore";
import styles from "./credentialsContainer.module.scss";
import Avatar from "../Avatar";
import profile from "../../assets/profile.jpg";

function CredentialsContainer() {
  const userInfo = useUserSettingsStore((state) => state.userInfo);
  return userInfo ? (
    <div className={styles.container}>
      <div className={styles.privet}>
        <p>Добрый вечер,</p>
        <h1>{userInfo.first_name}</h1>
      </div>
      <Link to={`/profile?type=${userInfo.type}`}>
        <Avatar src={profile} />
      </Link>
    </div>
  ) : (
    <>{/** @todo скелетонус */}</>
  );
}

export default CredentialsContainer;
