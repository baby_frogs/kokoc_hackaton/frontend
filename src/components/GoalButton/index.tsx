import styles from "./goalButton.module.scss";
import MainInfoButton from "../MainInfoButton";
import { ReactComponent as CircleSVG } from "../../assets/krug.svg";

function GoalButton() {
  return (
    <MainInfoButton
      p={<p>Цель</p>}
      src="/goals"
      style={{ gridArea: "B", position: "relative" }}
    >
      <div className={styles.bottom}>
        <h2>95%</h2>
        <p>Выполнено</p>
        <CircleSVG className={styles.circle} />
      </div>
    </MainInfoButton>
  );
}

export default GoalButton;
