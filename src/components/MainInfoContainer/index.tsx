import { useNavigate } from "react-router-dom";
import BigButton from "../../UI/BigButton";
import DailyStatsButton from "../DailyStatsButton";
import EarnedTodayButton from "../EarnedTodayButton";
import GoalButton from "../GoalButton";
import styles from "./mainInfoContainer.module.scss";

function MainInfoContainer() {
  const navigate = useNavigate();
  return (
    <div className={styles.container}>
      <EarnedTodayButton />
      <GoalButton />
      <DailyStatsButton />
      <BigButton
        style={{ gridArea: "D" }}
        color="black"
        text="Размяться"
        onClick={() => navigate("/trenya")}
      />
    </div>
  );
}

export default MainInfoContainer;
