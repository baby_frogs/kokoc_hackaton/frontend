import styles from "./ratingBlock.module.scss";

type Props = {
  title: string;
  value: string;
  place?: 1 | 2 | 3;
};

function RatingBlock({ title, value, place }: Props) {
  return (
    <div className={styles.container}>
      <p className={styles.title}>{title}</p>
      <p className={styles.value} data-place={place}>
        {value}
      </p>
    </div>
  );
}

export default RatingBlock;
