import styles from "./greetingsView.module.scss";
import heart from "../../assets/heart-big.png";
import BigButton from "../../UI/BigButton";
import { useNavigate } from "react-router-dom";

function GreetingsView() {
  const navigate = useNavigate();
  return (
    <div className={styles.container}>
      <img src={heart} alt="изображение сердца" />
      <div className={styles.text}>
        <h1>Привет 👋</h1>
        <p>Подключи пожалуйста приложение здоровья или введи свои данные!</p>
      </div>
      <div className={styles.buttons}>
        <BigButton text="Войти" onClick={() => navigate("/auth/signin")} />
        <BigButton
          text="Зарегистрироваться"
          color="white"
          onClick={() => navigate("/auth/signup")}
        />
      </div>
    </div>
  );
}

export default GreetingsView;
