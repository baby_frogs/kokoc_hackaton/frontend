import RatingBlock from "../../components/RatingBlock";
import styles from "./ratingView.module.scss";

function RatingView() {
  return (
    <div className={styles.container}>
      <h1>Рейтинг</h1>
      <div className={styles.ratingBlocks}>
        <RatingBlock title="1. Алексей Иванов" value="2 500" place={1} />
        <RatingBlock title="2. Михаил Алексеев" value="2 200" place={2} />
        <RatingBlock title="3. Сергей Михайлов" value="1 900" place={3} />
        <RatingBlock title="4. Артур Сергеев" value="1 700" />
        <RatingBlock title="5. Алексей Иванов" value="1 600" />
        <RatingBlock title="6. Михаил Алексеев" value="1 544" />
        <RatingBlock title="7. Сергей Михайлов" value="1000" />
        <RatingBlock title="8. Артур Сергеев" value="988" />
        <RatingBlock title="9. Алексей Иванов" value="985" />
        <RatingBlock title="10. Михаил Алексеев" value="900" />
      </div>
    </div>
  );
}

export default RatingView;
