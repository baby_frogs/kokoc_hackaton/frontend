import { useEffect } from "react";
import Header from "../../UI/Header";
import Achievements from "../../components/Achievements";
import Activities from "../../components/Activities";
import Challenges from "../../components/Challenges";
import CredentialsContainer from "../../components/CredentialsContainer";
import MainInfoContainer from "../../components/MainInfoContainer";
import Stories from "../../components/Stories";
import styles from "./mainView.module.scss";

function MainView() {
  return (
    <div className={styles.container}>
      <CredentialsContainer />
      <Stories />
      <MainInfoContainer />
      <Header text="Испытания" />
      <Challenges />
      <Header text="Активности" />
      <Activities />
      <Header text="Достижения" />
      <Achievements />
    </div>
  );
}

export default MainView;
