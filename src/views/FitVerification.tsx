import { useEffect } from "react";
import { verifyFit } from "../utils/axios";
import { useNavigate } from "react-router-dom";
import { useUserSettingsStore } from "../stores/userStore";

type Props = {};

function FitVerification({}: Props) {
  const navigate = useNavigate();
  const setFitnessAppAdded = useUserSettingsStore(
    (state) => state.setFitnessAppAdded
  );
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get("code");
    if (code)
      verifyFit(code).then((res) => {
        if (res) {
          setFitnessAppAdded(true);
          navigate("/main");
        } else {
          alert("Произошла ошибка");
          navigate("/addFit");
        }
      });
  });
  return <></>;
}

export default FitVerification;
