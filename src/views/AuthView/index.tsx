import { Outlet } from "react-router-dom";
import BackButton from "../../UI/BackButton";
import styles from "./authView.module.scss";

function AuthView() {
  return (
    <div className={styles.container}>
      <BackButton to="/greetings" />
      <Outlet />
    </div>
  );
}

export default AuthView;
