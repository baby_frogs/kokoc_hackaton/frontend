import { useEffect, useRef, useState } from "react";
import DropDownSelect from "../../UI/DropDownSelect";
import TextInput from "../../UI/TextInput";
import styles from "./authView.module.scss";
import BigButton from "../../UI/BigButton";
import { useUserSettingsStore } from "../../stores/userStore";
import { CompanyType } from "../../utils/typedefs";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function SingUpSecondStep() {
  const [companies, setCompanies] = useState<CompanyType[]>();
  useEffect(() => {
    axios
      .get(process.env.REACT_APP_BACKEND_ENDPOINT + "/api/companies")
      .then((res) => {
        setCompanies(res.data);
      });
  }, []);
  const [company, setCompany] = useState<CompanyType>();
  const [unit_in_company, setUnit] = useState<string>();
  const ref = useRef<HTMLFormElement>(null);
  const setCompanyCreds = useUserSettingsStore(
    (state) => state.setCompanyCreds
  );
  const signUpUser = useUserSettingsStore((state) => state.signUpUser);
  const navigate = useNavigate();
  return (
    <div className={styles.main}>
      <div className={styles.top}>
        <h1>Регистрация</h1>
        <p>Создайте учетную запись, чтобы следить за своей активностью</p>
      </div>
      <form ref={ref} className={styles.form}>
        {companies && (
          <DropDownSelect
            onChange={setCompany}
            placeholder="Выберите компанию"
            list={companies}
          />
        )}
        <TextInput
          placeholder="Должность, например Бос"
          type="text"
          disabled={!company ? true : false}
          required
          value={unit_in_company}
          onChange={(e) => setUnit(e.target.value)}
        />
      </form>
      <div className={styles.buttons}>
        <BigButton
          disabled={!ref.current?.checkValidity()}
          text="Продолжить"
          onClick={async () => {
            if (company && unit_in_company) {
              setCompanyCreds({ company: company.id, unit_in_company });
              if (await signUpUser()) navigate("/auth/signin");
            }
          }}
        />
      </div>
    </div>
  );
}

export default SingUpSecondStep;
