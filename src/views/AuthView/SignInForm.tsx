import { Link, useNavigate } from "react-router-dom";
import styles from "./authView.module.scss";
import BigButton from "../../UI/BigButton";
import TextInput from "../../UI/TextInput";
import { useUserSettingsStore } from "../../stores/userStore";
import { useRef, useState } from "react";
import { signin } from "../../utils/axios";

function SignInForm() {
  const navigate = useNavigate();
  const ref = useRef<HTMLFormElement>(null);
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();
  return (
    <div className={styles.main}>
      <div className={styles.top}>
        <h1>Авторизация</h1>
        <p>Войдите с учетной записью, чтобы попасть в приложение</p>
      </div>
      <form ref={ref} className={styles.form}>
        <TextInput
          required
          type="text"
          placeholder="E-mail"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextInput
          required
          placeholder="Пароль"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Link to="/forgotpassword">Забыли пароль?</Link>
      </form>
      <div className={styles.buttons}>
        <BigButton
          disabled={!(email && password) && !ref.current?.checkValidity()}
          text="Войти"
          onClick={async () => {
            if (email && password) {
              if (await signin({ email, password })) {
                navigate("/main");
              } else {
                alert("Возникла проблема!");
              }
            }
          }}
          onTouchEnd={async () => {
            if (email && password) {
              if (await signin({ email, password })) {
                navigate("/main");
              } else {
                alert("Возникла проблема!");
              }
            }
          }}
        />
        <BigButton
          text="Зарегистрировать фонд"
          color="white"
          onClick={() => navigate("auth/fund")}
          onTouchEnd={() => navigate("auth/fund")}
        />
      </div>
    </div>
  );
}

export default SignInForm;
