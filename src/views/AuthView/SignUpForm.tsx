import { useNavigate } from "react-router-dom";
import styles from "./authView.module.scss";
import BigButton from "../../UI/BigButton";
import TextInput from "../../UI/TextInput";
import { useRef, useState } from "react";
import { useUserSettingsStore } from "../../stores/userStore";

function SignUpForm() {
  const navigate = useNavigate();
  const [last_name, setSurname] = useState<string>();
  const [first_name, setName] = useState<string>();
  const [email, setEmail] = useState<string>();
  const [password, setPassword] = useState<string>();
  const [passwordAgain, setPasswordAgain] = useState<string>();
  const ref = useRef<HTMLFormElement>(null);
  const setCredentials = useUserSettingsStore((state) => state.setCredentials);
  return (
    <div className={styles.main}>
      <div className={styles.top}>
        <h1>Регистрация</h1>
        <p>Создайте учетную запись, чтобы следить за своей активностью</p>
      </div>
      <form ref={ref} className={styles.form}>
        <TextInput
          placeholder="Фамилия"
          value={last_name}
          required
          onChange={(e) => setSurname(e.target.value)}
        />
        <TextInput
          placeholder="Имя"
          value={first_name}
          required
          onChange={(e) => setName(e.target.value)}
        />
        <TextInput
          placeholder="E-mail"
          type="email"
          value={email}
          required
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextInput
          placeholder="Пароль"
          type="password"
          value={password}
          required
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextInput
          id="passwordAgain"
          placeholder="Повторите пароль"
          type="password"
          value={passwordAgain}
          required
          pattern={password}
          onChange={(e) => {
            setPasswordAgain(e.target.value);
          }}
        />
      </form>
      <div className={styles.buttons}>
        <BigButton
          disabled={!ref.current?.checkValidity()}
          text="Зарегистрироваться"
          onClick={() => {
            if (last_name && first_name && email && password && passwordAgain) {
              setCredentials({
                type: "user",
                last_name,
                first_name,
                email,
                password,
              });
              navigate("/auth/signup/proceed");
            }
          }}
        />
        <BigButton
          text="Зарегистрировать фонд"
          color="white"
          onClick={() => navigate("/auth/fund")}
        />
      </div>
    </div>
  );
}

export default SignUpForm;
