import styles from "./addFitView.module.scss";
import gear from "../../assets/gears.png";
import BigButton from "../../UI/BigButton";
import { getLink } from "../../utils/axios";
import { redirect, useNavigate } from "react-router-dom";
type Props = {};

function AddFitView({}: Props) {
  const navigate = useNavigate();
  return (
    <div className={styles.container}>
      <img className={styles.gear} src={gear} alt="изображение шестерёнки" />
      <div className={styles.text}>
        <h1>Подключите 🔗</h1>
        <p>
          Подключите пожалуйста приложение здоровья, чтобы следить за
          тренировками
        </p>
      </div>
      <div className={styles.button}>
        <BigButton
          color="black"
          text="Подключить Google Fit 🖤"
          onClick={async () => {
            const res = await getLink();
            if (res) window.open(res.url);
            else alert("Произошла ошибка");
          }}
        />
        <BigButton
          disabled
          color="white"
          text="Подключить Apple Health"
          onClick={() => {}}
        />
      </div>
    </div>
  );
}

export default AddFitView;
