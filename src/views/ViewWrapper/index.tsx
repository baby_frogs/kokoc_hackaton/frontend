import { PropsWithChildren } from "react";
import styles from "./viewWrapper.module.scss";
import Footer from "../../components/Footer";

interface Props extends PropsWithChildren {}

function ViewWrapper({ children }: Props) {
  return (
    <div className={styles.container}>
      {children}
      <Footer />
    </div>
  );
}

export default ViewWrapper;
