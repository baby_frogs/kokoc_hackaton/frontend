import { useNavigate } from "react-router-dom";
import BigButton from "../../UI/BigButton";
import styles from "./inDevView.module.scss";
import gear from "../../assets/gears.png";

type Props = {};

function InDevView({}: Props) {
  const navigate = useNavigate();
  return (
    <div className={styles.container}>
      <img src={gear} alt="изображение шестерёнки" />
      <div className={styles.text}>
        <h1>Страница в разарботке ⚙️</h1>
        <p>
          Привет, эта страница еще в разработке, но вы можете посетить другие
          прекрасные страницы нашего сервиса
        </p>
      </div>
      <div className={styles.buttons}>
        <BigButton
          text="Вернуться на главную"
          onClick={() => {
            navigate("/main");
          }}
        />
      </div>
    </div>
  );
}

export default InDevView;
