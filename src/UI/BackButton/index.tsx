import { Link } from "react-router-dom";
import styles from "./backButton.module.scss";
import { ReactComponent as ChevronLeft } from "../../assets/chevronLeft.svg";
type Props = {
  to: string;
};

function BackButton({ to }: Props) {
  return (
    <Link to={to} className={styles.container}>
      <ChevronLeft />
      <p>Назад</p>
    </Link>
  );
}

export default BackButton;
