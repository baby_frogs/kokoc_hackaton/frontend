import { Dispatch, SetStateAction, useEffect, useState } from "react";
import styles from "./dropDownSelect.module.scss";
type Props = {
  list: { id: number; name: string }[];
  placeholder: string;
  onChange?: Dispatch<SetStateAction<any>>;
};

function DropDownSelect({ list, placeholder, onChange }: Props) {
  const [current, setCurrent] = useState<{ id: number; name: string }>();
  const [opened, setOpened] = useState<boolean>(false);

  useEffect(() => {
    if (current && onChange) onChange(current);
  }, [current, onChange]);
  return (
    <div className={styles.container}>
      <button
        type="button"
        formNoValidate
        className={styles.button}
        onClick={() => setOpened((state) => !state)}
      >
        <p>{current?.name || placeholder}</p>
      </button>
      {opened && (
        <>
          <div className={styles.openedPart}>
            <ul className={styles.ul}>
              {list.map((li, id) => (
                <li
                  className={styles.li}
                  onClick={() => {
                    setCurrent(list[id]);
                    setOpened(false);
                  }}
                >
                  <p>{li.name}</p>
                </li>
              ))}
            </ul>
          </div>

          <div
            className={styles.overlay}
            onClick={() => setOpened(false)}
          ></div>
        </>
      )}
    </div>
  );
}

export default DropDownSelect;
