import { PropsWithChildren } from "react";
import styles from "./valueText.module.scss";

interface Props extends PropsWithChildren {}

function ValueText({ children }: Props) {
  return <h1 className={styles.text}>{children}</h1>;
}

export default ValueText;
