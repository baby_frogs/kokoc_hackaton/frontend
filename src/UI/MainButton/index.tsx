import React, { HTMLAttributes, PropsWithChildren } from "react";
import styles from "./mainButton.module.scss";

interface Props extends PropsWithChildren, HTMLAttributes<HTMLButtonElement> {}

function MainButton({ children, ...attrs }: Props) {
  return (
    <button className={styles.container} {...attrs}>
      {children}
    </button>
  );
}

export default MainButton;
