import styles from "./bigButton.module.scss";
import loader from "../../assets/loader.gif";
interface Props
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  text: string;
  fetch?: boolean;
  color?: "orange" | "white" | "black";
}

function BigButton({ text, fetch = false, color = "orange", ...attrs }: Props) {
  return (
    <button className={styles.container + " " + styles[color]} {...attrs}>
      {fetch ? (
        <img alt="loader" width="24px" height="24px" src={loader} />
      ) : (
        <p className={styles.text}>{text}</p>
      )}
    </button>
  );
}

export default BigButton;
