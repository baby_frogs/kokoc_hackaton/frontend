import styles from "./header.module.scss";
type Props = {
  text: string;
};

function Header({ text }: Props) {
  return <h1 className={styles.header}>{text}</h1>;
}

export default Header;
