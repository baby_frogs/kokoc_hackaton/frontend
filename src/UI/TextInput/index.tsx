import styles from "./textInput.module.scss";

interface Props
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  placeholder?: string;
}

function TextInput({ placeholder, ...attrs }: Props) {
  return (
    <input className={styles.input} placeholder={placeholder} {...attrs} />
  );
}

export default TextInput;
