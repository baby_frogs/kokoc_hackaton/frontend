import {
  createBrowserRouter,
  Navigate,
  Outlet,
  RouterProvider,
} from "react-router-dom";
import "./App.scss";
import MainView from "./views/MainView";
import ViewWrapper from "./views/ViewWrapper";
import RatingView from "./views/RatingView";
import AuthWrapper from "./components/AuthWrapper";
import GreetingsView from "./views/GreetingsView";
import AuthView from "./views/AuthView";
import SignInForm from "./views/AuthView/SignInForm";
import SignUpForm from "./views/AuthView/SignUpForm";
import SingUpSecondStep from "./views/AuthView/SingUpSecondStep";
import FitWrapper from "./components/FitWrapper";
import AddFitView from "./views/AddFitView";
import FitVerification from "./views/FitVerification";
import InDevView from "./views/InDevView";

function App() {
  const router = createBrowserRouter([
    {
      path: "/auth",
      element: <AuthView />,
      children: [
        {
          path: "/auth/signup",
          element: <SignUpForm />,
        },
        {
          path: "/auth/signup/proceed",
          element: <SingUpSecondStep />,
        },
        {
          path: "/auth/signin",
          element: <SignInForm />,
        },
      ],
    },
    {
      path: "/",
      errorElement: <InDevView />,
      element: (
        <AuthWrapper>
          <FitWrapper>
            <ViewWrapper>
              <Outlet />
            </ViewWrapper>
          </FitWrapper>
        </AuthWrapper>
      ),
      children: [
        {
          index: true,
          element: <Navigate to="/main" />,
        },
        {
          path: "/main",
          element: <MainView />,
        },
        {
          path: "/rating",
          element: <RatingView />,
        },
      ],
    },
    {
      path: "/greetings",
      element: <GreetingsView />,
    },
    {
      path: "/addFit",
      element: (
        <AuthWrapper>
          <AddFitView />
        </AuthWrapper>
      ),
    },
    {
      path: "/verify/fit",
      element: (
        <AuthWrapper>
          <FitVerification />
        </AuthWrapper>
      ),
    },
  ]);

  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
